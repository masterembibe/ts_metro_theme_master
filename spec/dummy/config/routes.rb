Rails.application.routes.draw do
  root :to => "home#index"
  get 'login' => "home#login"
  get 'status' => "home#status"
  post 'login' => "home#submit"
  post 'logout' => "home#logout"
  post 'reset' => "home#reset"
  mount TsMetroTheme::Engine => "/ts_metro_theme"

  get 'i' => "wrapper#interface", as: :interface
  get 'testontest' => "wrapper#testontest", as: :testontest
  get 'i/*page' => "wrapper#interface"
  get 'test' => "wrapper#test", as: :test
  get 'test/*page' => "wrapper#test"
  get 'l' => "wrapper#landing", as: :landing
  get 'ideacts' => "wrapper#ideacts", as: :ideacts
  get 'ideacts-password' => "wrapper#ideacts_password", as: :ideacts_password
  get 'chitkara' => "wrapper#chitkara", as: :chitkara
  get '100marks' => "wrapper#hundred_marks"
  get '100marks/test-series' => "wrapper#hundred_marks"
  get 'eptse' => "wrapper#eptse", as: :eptse
  get '/crack-icse' => "wrapper#crack_icse", as: :crack_icse
  get 'allen' => "wrapper#allen", as: :allen
  get 'physics-online-test' => "wrapper#physics_online_test"
  get 'jee-main-online-test-series' => "wrapper#jee_main_online_test_series"
  get 'jee-main-mock-tests' => "wrapper#jee_main_mock_tests"
  get 'jee-advance-online-test-series' => "wrapper#jee_advance_online_test_series"
  get 'jee-2014-paper' => "wrapper#jee_2014_solutions"
  get 'college-predictor' => "wrapper#college_predictor", as: :jee_college_predictor
  get 'chapter-tests' => "wrapper#chapter_tests", as: :chapter_tests
  get 'pricing' => "wrapper#pricing", as: :pricing
  get 'pricing_new' => "wrapper#pricing_new", as: :pricing_new

  get 'mock' => "wrapper#mock"
  get 'mock/*page' => "wrapper#mock"

  post 'landing_login' => 'wrapper#login', as: :landing_login
  post 'landing_signup' => 'wrapper#signup', as: :landing_signup
  post 'landing_password' => 'wrapper#password', as: :landing_password
  put 'landing_password' => 'wrapper#reset_password', as: :landing_reset_password

  mathjax 'mathjax'
end
