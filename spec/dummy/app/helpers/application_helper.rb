module ApplicationHelper

  def session_path(name)
    landing_login_path
  end

  def registration_path(name)
    landing_signup_path
  end

  def password_path(name)
    landing_password_path
  end
end
