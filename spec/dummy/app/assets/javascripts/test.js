/*global angular:false */
//= require angular
//= require angular-ui-router
//= require ts_metro_theme/test
//= require_tree ./util
//= require_self

angular.module('App', ['ui.router','TsMetroTheme.test', 'Util']).
  config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider){

  $urlRouterProvider.otherwise("/");

  $stateProvider.
    state('default',{
    url: '/',
    templateUrl: 'wrapper-stuff.html',
  }).
    state('login',{
    url: '/login',
    templateUrl: 'login.html',
  }).
    state('instructions',{
    url: '/instructions',
    templateUrl: 'instructions.html',
  }).
    state('start',{
    url: '/start',
    templateUrl: 'start.html',
  }).
    state('mock',{
    url: '/mock',
    templateUrl: 'mock.html',
  }).
    state('exit',{
    url: '/exit',
    templateUrl: 'exit.html',
  });


  $locationProvider.html5Mode(true);

}]).
  controller('Ctrl', ['$scope', function($scope) {
  $scope.ui = {};
}]);

