/*global angular:false */
//= require angular
//= require angular-ui-router
//= require ts_metro_theme/mock
//= require ts_metro_theme/interface
//= require_tree ./util
//= require_self

angular.module('App', ['ui.router', 'TsMetroTheme.interface', 'TsMetroTheme.mock', 'Util']).
  config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider){
    $urlRouterProvider.otherwise("/");

    $stateProvider.
    state('default',{
      url: '/',
      templateUrl: 'default.html'
    }).
    state('prof',{
      url: '/prof',
      templateUrl: 'profile-i.html',
    }).
   state('prof_',{
      url: '/prof_',
      templateUrl: 'profile-ii.html',
    }).
    state('feed',{
      url: '/feed',
      templateUrl: 'feed.html',
    }).
    state('mainpage',{
      url: '/main',
      templateUrl: 'mainpage.html',
    });

    $locationProvider.html5Mode(true);
  }]).

  controller('Ctrl', ['$scope', '$timeout', function($scope, $timeout) {
    $scope.ui = {};
    var State = {};
    $scope.State = State;

  $scope.ui.toggleVideoSolutionOverlayClass = "";
  $scope.ui.toggleVideoSolution = function toggleVideoSolution() {
    console.log("Hellow");
    if ($scope.ui.toggleVideoSolutionOverlayClass != "active") {
      $scope.ui.toggleVideoSolutionOverlayClass = "active";
    } else {
      $scope.ui.toggleVideoSolutionOverlayClass = "";
    }
  };

  $scope.ui.toggleKeyConceptOverlayClass = "";
  $scope.ui.toggleKeyConcept = function toggleKeyConcept() {
    console.log("Hellow");
    if ($scope.ui.toggleKeyConceptOverlayClass != "active") {
      $scope.ui.toggleKeyConceptOverlayClass = "active";
    } else {
      $scope.ui.toggleKeyConceptOverlayClass = "";
    }
  };

  $scope.ui.toggleAccordionClass = {
  };

  $scope.ui.toggleAccordionClass.maths = "";
  $scope.ui.toggleAccordionClass.physics = "open";
  $scope.ui.toggleAccordionClass.chemistry = "";

  $scope.ui.toggleAccordion = function toggleAccordion(msg) {
    if ($scope.ui.toggleAccordionClass[msg] != "open") {
      $scope.ui.toggleAccordionClass[msg] = "open";
    } else {
      $scope.ui.toggleAccordionClass[msg] = "";
    }
  };
}]);

