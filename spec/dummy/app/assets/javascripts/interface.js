/*global angular:false, _:false */
//= require angular
//= require ts_metro_theme/interface
//= require_tree ./util
//= require_self

angular.module('App', ['TsMetroTheme.interface', 'Util']).
  controller('Ctrl', ['$scope', '$timeout', '$q', function($scope, $timeout, $q) {
  $scope.ui = {};
  var State = {};

  State.cart = {};
  State.cart.count = 0;
  State.cart.categories = [ ];

  State.cart.categories[0] = {name: "Ultimate Test Series", items: [] };
  var exam = {name: "JEE (Main)", price: "889", message: "Congratulations! You're on your way to a great rank!", pitch: "Buying this is super important", selected: false };
  State.cart.categories[0].items.push(angular.copy(exam));
  exam.name = 'BITSAT';
  State.cart.categories[0].items.push(angular.copy(exam));
  exam.name = 'JEE Advanced';
  State.cart.categories[0].items.push(angular.copy(exam));

  State.cart.categories[1] = {name: "Revision Rocket", items: [] };
  var chapter = {name: "Units and Dimensions", price: "100", message: "Buy this", selected: false };
  State.cart.categories[1].items.push(angular.copy(chapter));

  State.testSeries = {};
  State.testSeries.packs = [];

  $scope.test = {
    fn: fn
  };
  function fn(){
    alert(angular.toJson(arguments));
    console.log('test.fn called', arguments);
  }

  $scope.ui.showAlert = function showAlert(msg) {
    $scope.ui.showAlertClass = "active";
    $scope.ui.showAlertMsg = msg;
    $timeout(function onTimeout() {
      $scope.ui.showAlertClass = "";
    }, 4000);
  };

  $scope.ui.showOverlayClass = "";
  $scope.ui.showOverlay = function showOverlay(msg) {
    $scope.ui.showOverlayClass = "active";
  };
  $scope.ui.hideOverlay = function hideOverlay(msg) {
    $scope.ui.showOverlayClass = "";
  };

  $scope.State = State;

  var getRandomSeries = function getRandomSeries() {
    return _.chain(10).range().map(function(n) {
      return [_.random(10,180), _.random(1, 10)];
    }).value();
  };

  $timeout(function(){
    $scope.flag = {
      user: 45,
      peerFast: 80,
      peerSlow: 20,
      flag: 90
    };
  }, 4 * 1000);

  $scope.QuestionPlot = {
    xAxis: {
      max: 180,
      tickInterval: 10
    }
  };

  $timeout(function() {
    $scope.QuestionPlot = {
      xAxis: {
        max: 18,
        tickInterval: 1
      }
    };
  }, 20 * 1000)

  $scope.ui.showInviteByEmail = false;

  $scope.saveInviteByEmail = function saveInviteByEmail() {
    console.log('saving is called', $scope.referral);
    var deferred = $q.defer();

    $timeout(function(){
     $scope. $apply(function() {
      deferred.resolve('testing');
     })
    }, 0);
    return deferred.promise;
  }

  $scope.referral = {};

  // To test show Invites
  // $timeout(function(){
  //   $scope.ui.showInviteByEmail = true;
  // }, 1 * 1000);
}]);

