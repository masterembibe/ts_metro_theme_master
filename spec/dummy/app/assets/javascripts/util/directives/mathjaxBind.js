/* global angular: false */
'use strict';

angular.module('Util').
  directive("mathjaxBind", ['$window',  function($window) {
  return {
    restrict: "A",
    controller: ["$scope", "$element", "$attrs", function($scope, $element, $attrs) {
      $scope.$watch($attrs.mathjaxBind, function(value) {
        var $loading = angular.element("<span class='loading-math-tex'>");
        $loading.html("...");
        var $script = angular.element("<span class='math-tex'>")
        .html(value === undefined ? "" : value);
        $element.html("");
        $script.addClass("ng-cloak");
        $element.append($loading);
        $element.append($script);
        if(angular.isUndefined($window.MathJax)) {
          alert("Failed to load question formatting utilities.  Please make sure you are online and reload.");
        } else {
          $window.MathJax.Hub.Queue(["Typeset", $window.MathJax.Hub, $element[0]], ["html", $loading, ""], ["removeClass", $script, 'ng-cloak' ]);
        }
      });
    }]
  };
}]);
