class HomeController < ApplicationController
  skip_before_filter :auth, except: [:reset]
  before_filter :notifications, only: [:index]

  def index
    redirect_to landing_path unless current_user
  end

  def login
  end

  def submit
    user_id = params[:user][:name].to_i
    password = params[:user][:password].to_s
    if user_id > 0 && user_id <= 20 && password == "theme123"
      cookies[:as_user] = user_id
      cookies[:at_time] =  Time.zone.now.to_i
      cookies[:with_secret] = "whatever #{cookies[:as_user]}#{cookies[:at_time]}".hash
      redirect_to root_path
    else
      flash[:alert] = "Could not authenticate as User #{user_id.inspect}."
      redirect_to login_path
    end
  end


  def logout
    cookies[:as_user] = cookies[:at_time] = cookies[:with_secret] = nil
    redirect_to root_path
  end

private
  def notifications
    @notifications ||= []
  end
end
