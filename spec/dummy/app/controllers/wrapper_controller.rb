class WrapperController < ApplicationController
  helper_method :page, :login_resource, :signup_resource, :forgot_resource, :reset_resource, :resource_name
  def test
    render layout: "test"
  end

  def ideacts
    render layout: "blank"
  end

  def crack_icse
    render layout: "landing"
  end

  def ideacts_password
    render layout: "blank"
  end

  def landing
    @user = user_resource
    render layout: "landing"
  end

  def pricing_new
    @user = user_resource
    render layout: "landing"
  end

  def chitkara
    @user = user_resource
    render layout: "chitkara"
  end

  def hundred_marks
    @user = user_resource
    render layout: "landing"
  end

  def eptse
    @user = user_resource
    render layout: "landing"
  end

  def allen
    @user = user_resource
    render layout: "allen"
  end

  def college_predictor
    @user = user_resource
    render layout: "landing"
  end

  def chapter_tests
    @user = user_resource
    render layout: "interface"
  end

  def pricing
    @user = user_resource
    render layout: "landing"
  end

  def physics_online_test
    @user = user_resource
    render layout: "landing"
  end

  def jee_main_online_test_series
    @user = user_resource
    render layout: "landing"
  end

  def jee_advance_online_test_series
    @user = user_resource
    render layout: "landing"
  end

  def jee_2014_solutions
    @user = user_resource
    render layout: "landing"
  end

  def jee_main_mock_tests
    @user = user_resource
    render layout: "landing"
  end

  def interface
    render layout: "interface"
  end

  def testontest
    render layout: "interface"
  end

  def mock
    render layout: "mock"
  end

  def login
    if params[:user][:login] == "user@embibe.com" && params[:user][:password] == "123456"
      redirect_to interface_path
    else
      @user = OpenStruct.new({errors: {}}.merge(params[:user])).tap do |result|
        err = result.errors
        def err.full_messages
          []
        end
      end
      flash[:alert] = "You need to sign in or signup before continuing."
      Rails.logger.info @user
      render action: 'landing', layout: 'landing'
    end
  end

  def signup
    if params[:user][:email] == "user@embibe.com" && params[:user][:password] == "123456"
      redirect_to interface_path
    else
      @user = OpenStruct.new({
        errors: {
          base: ["Error 1."],
          first_name: ["should be whatever."],
          email: ["should be user@embibe.com and password should be 123456"],
          confirm_password: ["does not match"]
        }
      }.merge(params[:user])).tap do |result|
        err = result.errors
        def err.full_messages
          r = []
          self.each do |k, v|
            if k.to_sym == :base
              r += v
            else
              r += v.map{|er| "#{k.to_s.humanize} #{er}"  }
            end
          end
          r
        end
      end
      Rails.logger.info @user
      render action: 'landing', layout: 'landing'
    end
  end

  def password
    if params[:user][:email] == "user@embibe.com"
      redirect_to landing_path(anchor: "reset-password")
    else
      @user = OpenStruct.new({
        errors: {
          base: ["Email should be user@embibe.com for this to work."]
        }
      }.merge(params[:user])).tap do |result|
        err = result.errors
        def err.full_messages
          r = []
          self.each do |k, v|
            if k.to_sym == :base
              r += v
            else
              r += v.map{|er| "#{k.to_s.humanize} #{er}"  }
            end
          end
          r
        end
      end
      Rails.logger.info @user
      render action: 'landing', layout: 'landing'
    end
  end

  def reset_password
    if params[:user][:password] == "123456" &&  params[:user][:password_confirmation] == "123456"
      redirect_to interface_path
    else
      @user = OpenStruct.new({
        errors: {
          base: ["Password should be 123456 for this to work."]
        }
      }.merge(params[:user])).tap do |result|
        err = result.errors
        def err.full_messages
          r = []
          self.each do |k, v|
            if k.to_sym == :base
              r += v
            else
              r += v.map{|er| "#{k.to_s.humanize} #{er}"  }
            end
          end
          r
        end
      end
      Rails.logger.info @user
      render action: 'landing', layout: 'landing'
    end
  end
  private
  def page
    case params["action"]
    when "reset_password" then "reset-password"
    when "password" then "forgot-password"
    when "login" then "login"
    when "signup" then "signup"
    else "landing"
    end
  end

  def user_resource
    OpenStruct.new(errors: {})
  end

  def reset_resource
    return user_resource unless page == "reset-password"
    @user
  end

  def forgot_resource
    return user_resource unless page == "forgot-password"
    @user
  end

  def login_resource
    return user_resource unless page == "login"
    @user
  end

  def signup_resource
    return user_resource unless page == "signup"
    @user
  end

  def resource_name
    :user
  end
end
