class ApplicationController < ActionController::Base
  protect_from_forgery
  helper_method :current_user
  before_filter :auth, :store_request_token

  private

  def store_request_token
    if params[:request_ids]
      session[:request_ids] = params[:request_ids]
    end
    @request_ids = session[:request_ids]
  end

  def auth
    @current_user = OpenStruct.new(id: 1, name: "Fake user 1")
    return true if current_user
    flash[:alert] = "Are you an embibe tester?  Prove it."
    redirect_to "/"
    false
  end

  def current_user
    return @current_user if @current_user
    cookie_check
    @current_user = OpenStruct.new(id: cookies[:as_user].to_i, name: "Fake user #{cookies[:as_user]}") if cookies[:as_user]
    if !Rails.env.production?
      Rails.logger.info "~~~~~~~~~~~~~~~~~~~~~~~~~ #{params[:as_user].to_i.zero? ? "Not O" : "O"}verriding current_user from params=#{params.inspect}"
      @current_user = OpenStruct.new(user_id: cookies[:as_user].to_i) if params[:as_user].to_i > 0
      if params[:set_cookies]
        cookies[:as_user] = @current_user.id
        cookies[:at_time] =  Time.zone.now.to_i
        cookies[:with_secret] = "whatever #{cookies[:as_user]}#{cookies[:at_time]}".hash
      end
    end
    @current_user
  end

  def cookie_check
    if cookies[:as_user] && cookies[:at_time] && cookies[:with_secret]
      return true if cookies[:with_secret].to_s == "whatever #{cookies[:as_user]}#{cookies[:at_time]}".hash.to_s &&  (Time.zone.now.to_i - cookies[:at_time].to_i) <= 9600
      flash[:info] = "Cookie expired. Login again." if cookies[:as_user].to_i > 0
    end
    cookies[:as_user] = cookies[:at_time] = cookies[:with_secret] = nil
  end

end
