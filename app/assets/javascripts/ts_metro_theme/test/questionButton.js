/* global angular: false */

angular.module('TsMetroTheme.test').
  directive('questionButton', function() {
  return {
    restrict: 'E',
    scope: {
      type: '@',
      value: '@',
    },
    controller: ['$scope', function questionButtonCtrl($scope) {
      $scope.textMap = {
        not_visited: 'Not Visited',
        not_answered: 'Not Answered',
        created: 'Created',
        visited: 'Visited',
        answered: 'Answered',
        review: 'Review',
        review_answered: 'Review Answered',
        finished: 'Finished',
        discarded: 'Discarded'
      };
      $scope.classMap = {
        created: 'not_visited',
        visited: 'not_answered',
        answered: 'answered',
        review: 'review',
        review_answered: 'review_answered',
        finished: 'discarded',
      };
    }],
    templateUrl: 'TsMetroTheme.test.questionButton.html'
  };
});

