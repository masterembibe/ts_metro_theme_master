/* global angular: false */

angular.module('TsMetroTheme.test').
  directive('instructions', function() {
  return {
    restrict: 'E',
    scope: {
      type: '@',
      exam: '=',
      duration: '=?',
      questions: '=?',
      sections: '=?',
      name: '=',
      category: '=',
      instructionType: '='
    },
    transclude: true,
    templateUrl: "TsMetroTheme.test.instructions.html",
    controller: ['$scope', '$element', '$attrs', function($scope, $element, $attrs) {
      $scope.$watch('name', function(newVal, oldVal) {
        if(newVal) {
          $scope.sequence = newVal.slice(-1) || "A";
        }
      });
      $scope.$watch('sections', function(newVal, oldVal) {
        if(angular.isDefined(newVal)) {
        	$scope.subjects = [];
          _.each(newVal, function(key, item){
          	if(item != 'default'){
          		$scope.subjects.push(item);
              $scope.subjectList = [$scope.subjects.slice(0, $scope.subjects.length-1).join(', ')||'', $scope.subjects[$scope.subjects.length-1]].filter(function(e) { return e; }).join(' and ');

          	}
          })
        }
      });
    }]
  };
});
