/* global angular: false */

angular.module('TsMetroTheme.test').
  directive('sectionButton', function() {
  return {
    restrict: 'E',
    scope: {
      name: '@',
      status: '@',
      stats: '=',
    },
    templateUrl: "TsMetroTheme.test.sectionButton.html"
  };
});

