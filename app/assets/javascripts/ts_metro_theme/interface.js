//= require ts_metro_theme/lodash
//= require ts_metro_theme/standalone-framework
//= require ts_metro_theme/highcharts
//= require_self
//= require_tree ./interface
//= require ts_metro_theme/ui-bootstrap-tpls
/* global angular: false */
'use strict';

angular.module('TsMetroTheme.interface', ['ui.bootstrap'])
  .run(['$document', '$rootScope', function($document, $rootScope) {
    $document.on('keydown', function(event) {
      if (event.which == 27) {
        $rootScope.$broadcast('EscapeFired');
        $rootScope.$apply();
      };
    });
  }]);
