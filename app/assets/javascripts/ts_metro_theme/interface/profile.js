/* global angular: false */

angular.module('TsMetroTheme.interface').
  directive('profile', function() {
  return {
    restrict: 'E',
    scope: {
      src: '@',
      name: '@',
      upload: '=',
      airPrediction: '@',
      exams: '=',
      completion: '=',
      tooltipMsg: '@'
    },
    templateUrl: "TsMetroTheme.interface.profile.html",
    transclude: true,
    controller: ['$scope', '$element', '$attrs', '$timeout', function ($scope, $element, $attrs, $timeout) {
        $scope.$watch('upload', function(newValue, oldValue) {
          if(angular.isNumber(newValue)) {
            $scope.uploading = true;
          } else {
            $scope.uploading = false;
          }
        });
        $scope.ui = {};
        $scope.$watch('completion', function(newValue, oldValue) {
          if(oldValue && newValue != oldValue) {
            $scope.ui.fakeTooltipVisible = 'active';
            $timeout(function(){
              $scope.ui.fakeTooltipVisible = '';
            }, 3000)
          }
        });
    }]
  };
});