/* global angular: false, _: false */

angular.module('TsMetroTheme.interface').
  directive('flagPlot', ['$log', function($log) {

  var prepareStyle = function prepareStyle(value) {
    return {left: Math.max(_.parseInt(value, 10),0)*0.92 + '%'};
  };

  return {
    restrict: 'E',
    templateUrl: "TsMetroTheme.interface.flagPlot.html",
    scope: {
      user: '=',
      topper: '=',
      average: '=',
      flag: '=',
      stats: '=',
      labels: '=',
      category: '=',
      medical: '=?'
    },
    controller: ['$scope', '$element', '$attrs', function ($scope, $element, $attrs) {

      $scope.$watch('flag', function(newValue) {
        $scope.flagStyle = prepareStyle(newValue);
      });

      $scope.$watch('user', function(newValue, OldValue) {
        if (newValue || newValue === 0) {
          $scope.userStyle = prepareStyle(newValue);
        }
      });

      $scope.$watch('topper', function(newValue, OldValue) {
        if (newValue || newValue === 0) {
          $scope.peerFastStyle = prepareStyle(newValue);
        }
      });

      $scope.$watch('average', function(newValue, OldValue) {
        if (newValue || newValue === 0) {
          $scope.peerSlowStyle = prepareStyle(newValue);
        }
      });
    }]
  };
}]);