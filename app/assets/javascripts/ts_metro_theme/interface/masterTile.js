/* global angular: false */

angular.module('TsMetroTheme.interface').
  directive('masterTile', function() {
  return {
    restrict: 'E',
    scope: {
      title: '@',
      status: '=',
      heading: '@',
      showCompare: '=?',
      unlockFn: '&',
      compareFn: '&'
    },
    templateUrl: "TsMetroTheme.interface.masterTile.html",
    controller: ['$scope', function($scope){
    	$scope.lockFn = $scope.unlockFn;
    	$scope.compFn = $scope.compareFn;
    }]
  };
});