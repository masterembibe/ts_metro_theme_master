/* global angular: false */

angular.module('TsMetroTheme.interface').
  directive('slidingTile', function() {
  return {
    restrict: 'E',
    scope: {
      type: '@',
      count: '=',
    },
    controller: ['$scope', '$templateCache', '$element', function slidingTileCtrl($scope, $templateCache, el) {
      $scope.imageMap = angular.fromJson($templateCache.get('TsMetroTheme.interface.slidingTile.imageMap.json'));
      $scope.textMap = {
        embibium: 'Perfect Attempts',
        timeTrolls: 'Overtimes',
        wastedAttempts: 'Wasted Attempts',
        strongChapters: 'Strong Chapters',
        cartItems: 'Items in Cart',
        friendsJoined: 'Friends Joined'
      };

      el.bind("click", function(e) {
        el.parent().parent().children().removeClass('active');
        el.parent().addClass('active');
      })

      el.bind("mouseenter", function(e) {
        el.parent().parent().children().removeClass('active');
      })

    }],
    templateUrl: "TsMetroTheme.interface.slidingTile.html"
  };
});

