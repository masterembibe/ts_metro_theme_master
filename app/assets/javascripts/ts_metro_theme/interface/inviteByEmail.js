angular.module('TsMetroTheme.interface').
  directive('inviteByEmail', ['$log', '$timeout', function($log, $timeout) {

    return {
      restrict: 'E',
      templateUrl: 'TsMetroTheme.interface.inviteByEmail.html',
      scope: {
        referralModel: '=',
        save: '&'
      },
      controller: ['$scope', '$element', '$attrs', function($scope, $element, $attrs) {
        $scope.addMore = function addMore() {
          var forms = $element.find('form');
          current_form = angular.element(forms[0]);
          if (current_form.hasClass('prev')) {
            clone_form = angular.element(forms[0]);
            current_form = angular.element(forms[1])
          } else  {
            clone_form = angular.element(forms[1]);
          }
          $scope.save().then(function() {
            current_form.addClass('prev send-left').removeAttr('style');
            clone_form.removeClass('prev send-left').css('left', '36%');
            current_form.removeClass('send-left');
            $scope.referralModel = {};
          });
        }
      }]
    }

  }]);