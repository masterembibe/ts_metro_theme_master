/* global angular: false, _: false */

angular.module('TsMetroTheme.interface').
  directive('sectionSummaryStats', [function() {

  return {
    restrict: 'E',
    templateUrl: "TsMetroTheme.interface.sectionSummaryStats.html",
    scope: {
      section: '@',
      stats: '='
    }
  };
}]);