/* global angular: false */

angular.module('TsMetroTheme.interface').
  directive('profileImage', function() {
  return {
    restrict: 'E',
    scope: {
      name: '@',
      src: '@',
    },
    templateUrl: "TsMetroTheme.interface.profileImage.html"
  };
});

