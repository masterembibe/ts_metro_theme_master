/* global angular: false */

angular.module('TsMetroTheme.interface').
  directive('globalNav', function() {
  return {
    restrict: 'E',
    scope: {
      userPic: "=?",
      subdomainPic: "=?",
      statusInfo: "=?",
      categories: "=?",
      activeCategory: "=?",
      navItems: "=?",
      clickFn: "=?",
      profileUrl: "=?",
      stats: "=?"
    },
    templateUrl: "TsMetroTheme.interface.globalNav.html"
  };
});

