/* global angular: false */

angular.module('TsMetroTheme.interface').
  directive('flair', function() {
  return {
    restrict: 'E',
    scope: {
      type: '@',
      count: '=',
    },
    controller: ['$scope', function slidingTileCtrl($scope) {
      $scope.name = {
        advanced: 'IITian',
        main: 'Rocked Mains',
        bitsat: 'BITSAT Ace'
      };
      $scope.officialName = {
        advanced: 'JEE Advanced',
        main: 'JEE Main',
        bitsat: 'BITSAT Ace'
      };
    }],
    templateUrl: "TsMetroTheme.interface.flair.html"
  };
});


