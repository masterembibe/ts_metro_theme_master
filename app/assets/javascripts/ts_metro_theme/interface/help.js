/* global angular: false */
'use strict';

angular.module('TsMetroTheme.interface').
  directive('help', function() {
  return {
    restrict: 'E',
    scope: {
      visible: '=?'
    },
    templateUrl: 'TsMetroTheme.interface.help.html',
    controller: ['$scope', '$element', '$templateCache', '$interval', function($scope, el, $templateCache, $interval){
      $scope.imageMap = angular.fromJson($templateCache.get('TsMetroTheme.interface.introEl.imageMap.json'));
      var tourOverlay = el.find('div');

      $scope.$watch('visible', renderHelp);

      function renderHelp() {
        if(angular.isDefined($scope.helpPol)) {
          $interval.cancel($scope.helpPol);
          $scope.helpPol = undefined;
          if(!$scope.visible) {
            tourOverlay.empty();
            return;
          }
        }
        if($scope.visible) {
        	$scope.helpPol = $interval(showHelps, 1000);
        }
      }

       function showHelps() {
          tourOverlay.html('');
          var elements = document.querySelectorAll('[help-el]');
          //console.log(elements.length);
          for (var j = elements.length - 1; j >= 0; j--) {
            var element = angular.element(elements[j]);
            if(element.attr('help-el') == 'true') {
              var uniqId = element.attr('help-id') ? element.attr('help-id') : Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1);
              element.attr('help-id', uniqId);
              showEl(element, uniqId);
            }
          }
        }


      function showEl(elem, uniqId) {
        var helperPos   = elem.attr('help-pos') ? elem.attr('help-pos') : 'bottom';
        var helperXY    = {};
        var off         = elem[0].getBoundingClientRect();
        var offTop      = parseInt(elem.attr('help-offtop') ? elem.attr('help-offtop') : 0);
        var offLeft     = parseInt(elem.attr('help-offleft') ? elem.attr('help-offleft') : 0);
        var helpClass  = elem.attr('help-class') ? elem.attr('help-class') : "";

        if (helperPos === 'bottom') {
          helperXY['top'] = off.top + off.height;
          helperXY['left'] = off.left;
        } else if (helperPos === 'right'){
          helperXY['top'] = off.top + off.height;
          helperXY['left'] = off.left + off.width;
        } else if (helperPos === 'left'){
          helperXY['top'] = off.top + off.height;
          helperXY['left'] = off.left;
        } else if (helperPos === 'top'){
          helperXY['top'] = off.top;
          helperXY['left'] = off.left;
        }

        helperXY['top'] +=  offTop + window.scrollY;
        helperXY['left'] += offLeft;

        var marker = angular.element(document.createElement('img'))
          .attr('src', $scope.imageMap['right'])
          .addClass('marker');
        var helpMsg = angular.element(document.createElement('p'))
          .addClass('help-msg')
          .html(elem.attr('help-msg'));
        var helperDiv = angular.element(document.createElement('div'))
          .addClass('help-helper marker-' + helperPos + " " + helpClass)
          .append(marker)
          .append(helpMsg)
          .attr('help-id', uniqId)
          .css({
            'position': 'absolute',
            'top': helperXY['top']+'px',
            'left': helperXY['left']+'px'
          });
        var tourElements = tourOverlay.children();
        if(!tourElements.length) {
          tourOverlay.append(helperDiv);
          return;
        }
        for (var i = tourElements.length - 1; i >= 0; i--) {
          var elem = angular.element(tourElements[i]);
          if(elem.attr('help-id') == uniqId) {
            elem.remove();
          }
          tourOverlay.append(helperDiv);
        };
      }
    }],
  };
});