/* global angular: false */

angular.module('TsMetroTheme.interface').
  directive('stat', function() {
  return {
    restrict: 'E',
    scope: {
      type: '@',
      count: '=',
    },
    controller: ['$scope', function statCtrl($scope) {
      $scope.description = {
        perfect: 'These are all the questions you got right within ideal time! Go to each test.',
        strong: 'These are your high speed, high accuracy chapters. Identify your strong chapters by practicing them in Revison Rocket.',
        overtime: 'These are all the questions you went over time on.',
        wasted: 'These are all the questions you got wrong and spent very little time on.',
      };
      $scope.name = {
        perfect: "Perfect attempts",
        strong: "Strong chapters",
        overtime: "Over times",
        wasted: "Wasted attempts",
      }
    }],
    templateUrl: "TsMetroTheme.interface.stat.html"
  };
});

