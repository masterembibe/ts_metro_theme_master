/*global angular:false, _:false, Highcharts: false */
/* jshint camelcase: false */
//= require ts_metro_theme/d3

angular.module('TsMetroTheme.interface').
  directive('testOnTest', function() {
    var drawBg = function drawBg(svg, width, height) {
      var bg = svg.append('g').attr('class','test-on-test__rect');
      bg.append('rect')
        .attr('class', 'test-on-test__bg')
        .attr('x', 0)
        .attr('y', 0)
        .attr('width', function(){
          return width;
        })
        .attr('height', function(){
          return height;
        });
    };

    return {
      restrict: 'E',
      scope: {
        attempts: '=?',
        xaxis: '=?',
        label: '=?',
        width: '=?',
        height: '=?',
        domain: '=?',
        unit: '=?'
      },
      templateUrl: 'TsMetroTheme.interface.testOnTest.html',
      controller: ['$scope', '$element', function($scope, $element) {

        $scope.$watch('attempts', function(data) {
          if(!angular.isDefined(data)) { return; }

          $scope.width = angular.isDefined($scope.width) ? $scope.width : 960;
          $scope.height = angular.isDefined($scope.height) ? $scope.height : 400;

          var margin = {top: 20, right: 20, bottom: 30, left: 55},
            width = $scope.width - margin.left - margin.right,
            height = $scope.height - margin.top - margin.bottom;

          var svg = d3.select($element.find('svg')[0])
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');


          var x = d3.scale.linear()
            .domain([0, data[0].points.length])
            .range([0, width-20]);

          var y = d3.scale.linear()
            .range([height, 0]);

          var min = data[0].points[0].value;
          var max = 0;

          for( var i in data ) {
            var index = data[i].points.length;
            for (var j = data[i].points.length - 1; j >= 0; j--) {
              data[i].points[j].x = index;
              min = Math.min(min, data[i].points[j].value);
              max = Math.max(max, data[i].points[j].value);
              index--;
            };
          }

          if(!angular.isDefined($scope.domain)) {
          	$scope.domain = {
          		'max': max,
          		'min': min
          	}
          }

          y.domain([$scope.domain.min - ($scope.domain.max - $scope.domain.min)/10, $scope.domain.max + ($scope.domain.max - $scope.domain.min)/10]);

          var yTicks = [];
          for (var i = 0; i <= 5; i++) {
            yTicks.push($scope.domain.min + i*(($scope.domain.max - $scope.domain.min)/5));
          };
          //yTicks.push($scope.domain.min - ($scope.domain.max - $scope.domain.min)/5);
          //yTicks.push($scope.domain.max + ($scope.domain.max - $scope.domain.min)/5);

          var tickLength = data[0].points.length + 1;
          var xAxis = d3.svg.axis()
            .scale(x)
            .orient('bottom')
            .ticks(tickLength)
            .tickFormat(function(i){
              try {
                var len = Math.ceil(width/(data[0].points.length*9));
                if(i == 0 || i == data[0].points.length + 1) {
                  return '';
                } else if (len < data[0].points[i-1].label.length) {
                  return data[0].points[i-1].label.substring(0,len) + '..';
                } else {
                  return data[0].points[i-1].label;
                }
              } catch(e) {
                return '';
              }
            });

          var yAxis = d3.svg.axis()
            .scale(y)
            .orient('left')
            .ticks(7)
            .tickValues(yTicks);

          svg.append('g')
              .attr('class', 'x axis')
              .attr('transform', 'translate(0,' + height + ')')
              .call(xAxis);

          svg.append('g')
              .attr('class', 'y axis')
              .call(yAxis);

          svg.selectAll('.x.axis text')
            .on('click', function(d,i){
              //if(!d.url){ return; }
              var win = window.open(data[0].points[i-1].url, '_blank');
              win.focus();
            })

          var line = d3.svg.line()
            .x(function(d, i) { return x(d.x); })
            .y(function(d) { return y(d.value) - 2*d.slip; })
            .interpolate('monotone');

          drawBg(svg, width, height);

          for( var i in data ) {
            svg.selectAll('circle.point'+'.point'+i)
              .data(data[i].points)
              .enter()
              .append('circle')
              .attr('class', 'point point'+i)
              .attr('cx', function(d,i){
                return x(d.x)+'px';
              })
              .attr('cy', function(d,k){
                return y(d.value) - 2*i +'px';
              })
              .attr('label', function(){
                return data[i].label;
              })
              .attr('r', 7)
              .attr('fill', function(d){
              	console.log(i,data[i].color);
                if(d.value != null) {
                  return data[i].color;
                } else {
                	return '#444444'
                }
              })
              .on('mouseenter', function(d,j){
                if(d.value === null) {
                	$scope.tooltip = d;
                  $scope.tooltip.msg = 'You haven\'t looked at all questions for this test';
                  $scope.tooltip.color = this.getAttribute('fill');
                  $scope.tooltip.label = this.getAttribute('label');
                  $scope.tooltip.top = y(0) + 40;
                  $scope.tooltip.left = x(d.x) + 47;
                  $scope.$digest();
                  return;
                }
                $scope.tooltip = {}
                if(d.tooltip) {
                  $scope.tooltip = d;
                }
                $scope.tooltip.value = d.value;
                $scope.tooltip.color = this.getAttribute('fill');
                $scope.tooltip.label = this.getAttribute('label');
                if($scope.unit) {
                	$scope.tooltip.value += ' ' + $scope.unit;
                }
                $scope.tooltip.top = y(d.value) + 40;
                $scope.tooltip.left = x(d.x) + 48;
                $scope.$digest();
              })
              .on('mouseleave', function(d,i){
                $scope.$apply(function(){
	                $scope.tooltip = null;
                });
              });

            var arr = [];
            for (var k = 0; k < data[i].points.length; k++) {
              data[i].points[k].slip = i;
              if(data[i].points[k].value !== null) {
                arr.push(data[i].points[k]);
              }
            };

            svg.select('.test-on-test__rect')
            	.append('path')
              .datum(arr)
              .attr('class', 'line')
              .attr('d', line)
              .style('stroke', data[i].color);
          }
        });
      }]
    }
  });