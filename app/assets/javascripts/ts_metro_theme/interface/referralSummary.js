/* global angular: false */

angular.module('TsMetroTheme.interface').
  directive('referralSummary', function() {
  return {
    restrict: 'E',
    scope: {
      type: '@',
      crystalsPerType: '@',
      number: '=',
    },
    templateUrl: "TsMetroTheme.interface.referralSummary.html"
  };
});

