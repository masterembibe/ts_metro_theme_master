/* global angular: false */

angular.module('TsMetroTheme.interface').
  directive('pointsEarned', function() {
  return {
    restrict: 'E',
    scope: {
      type: '@',
      count: '=',
    },
    controller: ['$scope', '$templateCache', function pointsEarnedCtrl($scope, $templateCache) {
      $scope.imageMap = angular.fromJson($templateCache.get('TsMetroTheme.interface.pointsEarned.imageMap.json'));
      $scope.textMap = {
        embibium: 'Perfect Attempts',
        timeTrolls: 'Overtimes',
        wastedAttempts: 'Wasted Attempts',
      };
    }],
    templateUrl: "TsMetroTheme.interface.pointsEarned.html"
  };
});

