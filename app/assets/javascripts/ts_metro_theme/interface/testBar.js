/*global angular:false, _:false, Highcharts: false */
/* jshint camelcase: false */


angular.module('TsMetroTheme.interface').
  directive('testBar', function() {
  return {
    restrict: 'E',
    scope: {
      value: "=?",
      layout: "@",
      color: "=?",
      tickColor: "=?",
      ticks: "=?",
      width: "=?",
      multi: "=?"
    },
    transclude: true,
    templateUrl: "TsMetroTheme.interface.test_bar.html",
    controller: ['$scope', '$element', function testSummaryCtrl($scope) {
      $scope.layout = $scope.layout ? $scope.layout : 'horizontal';
      $scope.$watch('ticks', onTickChange);

      function onTickChange(newVal) {
        $scope.tickArr = [];
        if(angular.isDefined(newVal)) {
          $scope.tickInterval = 100/newVal;
          for (var i = 1; i <= newVal; i++) {
            $scope.tickArr.push(i);
          }
        } else { $scope.ticks = 10; }
      }
    }],
  };
});
