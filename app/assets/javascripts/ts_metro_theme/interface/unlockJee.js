/* global angular: false */

angular.module('TsMetroTheme.interface').
  directive('unlockJee', function() {
  return {
    restrict: 'E',
    scope: {
      count: '=',
    },
    controller: ['$scope', function unlockJeeCtrl($scope) {
      $scope.unlockJee = $scope.count > 2 ? true : false;
      var counter = $scope.count;
      if (counter > 3) { counter = 3; }
    }],

    templateUrl: "TsMetroTheme.interface.unlockJee.html"
  };
});

