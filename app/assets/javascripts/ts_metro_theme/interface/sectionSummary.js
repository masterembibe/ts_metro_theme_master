/* global angular: false, _: false */

angular.module('TsMetroTheme.interface').
  directive('sectionSummary', [function() {

  return {
    restrict: 'E',
    templateUrl: "TsMetroTheme.interface.sectionSummary.html",
    scope: {
      section: '@',
      stats: '='
    }
  };
}]);