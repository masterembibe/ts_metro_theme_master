/* global angular: false */

angular.module('TsMetroTheme.interface').
  directive('breakdown', function() {
  return {
    restrict: 'E',
    scope: {
      attempts: "=?",
      legend: "=?",
      yaxis: "=?",
      domain: "=?",
      unit: '=?',
      chapters: '=?'
    },
    templateUrl: 'TsMetroTheme.interface.breakdown.html',
    controller: ['$scope', '$element', function($scope, el){
      $scope.$watch('attempts', function(){
        if(!angular.isDefined($scope.attempts)) { return; }
        var min = 0,
            max = 0;
        for (var i in $scope.attempts) {
          for (var j = 2; j >= 0; j--) {
            max = Math.max(max, $scope.attempts[i].result[j]);
          };
        };
        max = max + (max-min)/10;

        if(angular.isDefined($scope.domain)) {
        	min = $scope.domain.min;
        	max = $scope.domain.max;
        }

        for (var i in $scope.attempts) {
        	$scope.attempts[i].newResult = {};
          for (var j = 2; j >= 0; j--) {
            $scope.attempts[i].newResult[j] = ($scope.attempts[i].result[j]/max)*100;
          }
        }

        $scope.ticks = [];
        for (var i = 5; i >= 0; i--) {
          $scope.ticks.push(Math.round(min + i*((max-min)/5)));
        };
      });
    }]
  }
});

