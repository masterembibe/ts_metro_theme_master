/* global angular: false, _: false */

angular.module('TsMetroTheme.interface').
  directive('sectionSummaryAttempts', [function() {

  return {
    restrict: 'E',
    templateUrl: "TsMetroTheme.interface.sectionSummaryAttempts.html",
    scope: {
      section: '@',
      stats: '='
    }
  };
}]);