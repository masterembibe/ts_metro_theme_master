/* global angular: false */

angular.module('TsMetroTheme.interface').
  directive('testSummary', function() {
  return {
    restrict: 'E',
    scope: {
      type: '@',
      score: '=',
      coverage: '@',
    },
    controller: ['$scope', function testSummaryCtrl($scope) {
      $scope.name = {
        physics: "Physics",
        chemistry: "Chemistry",
        maths: "Maths",
      };
    }],
    templateUrl: "TsMetroTheme.interface.testSummary.html"
  };
});

