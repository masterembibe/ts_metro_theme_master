/* global angular: false */
/* jshint maxlen: 200 */

angular.module('TsMetroTheme.interface').
  directive('flipTile', function() {
  return {
    restrict: 'AE',
    scope: {
      status: '=',
      heading: '@',
      message: '@',
      price: '@',
      stats: '=',
      slug: '=',
      isImportant: '=',
      isPartTest: '=?',
      chapters: '=?',
      isLive: '=?',
      backCountdownInit: '=?',
      frontCountdownInit: '=?',
      frontMessage: '=?',
      frontCountdowTrigger: '&',
      backCountdownTrigger: '&'
    },
    templateUrl: 'TsMetroTheme.interface.flipTile.html',
    controller: ['$scope', '$interval', function($scope, $interval){
      var countdownTimer;
      function resetTimers() {
        if(countdownTimer) { $interval.cancel(countdownTimer); countdownTimer = false; }
        countdownTimer = $interval(everySecond, 1000);
        $scope.frontCountdown = $scope.frontCountdownInit || 0;
        $scope.backCountdown = $scope.backCountdownInit || 0;
      }

      $scope.$on('destroy', onDestroy);

      function onDestroy() {
        if(countdownTimer) { $interval.cancel(countdownTimer); countdownTimer = false; }
      }

      function everySecond() {
        $scope.frontCountdown = $scope.frontCountdown - 1;
        $scope.backCountdown = $scope.backCountdown - 1;
        $scope.front = {
          hours: Math.floor(($scope.frontCountdown/3600)),
          minutes: Math.floor(($scope.frontCountdown/60)%60),
          seconds: Math.floor($scope.frontCountdown%60)
        };
        $scope.back = {
          hours: Math.floor(($scope.backCountdown/3600)),
          minutes: Math.floor(($scope.backCountdown/60)%60),
          seconds: Math.floor($scope.backCountdown%60)
        };
        if($scope.frontCountdown <= 0 && $scope.backCountdown <= 0) {
          onDestroy();
        }
        if(Math.floor($scope.frontCountdown) <= 0) {
          $scope.front = { hours: 0, minutes: 0, seconds: 0 };
        }
        if(Math.floor($scope.frontCountdown) === 0) {
          if(!angular.isUndefined($scope.frontCountdownInit) && angular.isFunction($scope.frontCountdownTrigger)) { $scope.frontCountdownTrigger(); }
        }

        if(Math.floor($scope.backCountdown) <= 0) {
          $scope.back = { hours: 0, minutes: 0, seconds: 0 };
        }
        if(Math.floor($scope.backCountdown) === 0) {
          if(!angular.isUndefined($scope.backCountdownInit) && angular.isFunction($scope.backCountdownTrigger)) { $scope.backCountdownTrigger(); }
        }
      }

      $scope.$watch('frontCountdownInit', resetTimers);
      $scope.$watch('backCountdownInit', resetTimers);

    }]
  };
});