/*global angular:false, _:false, Highcharts: false */
/* jshint camelcase: false */

angular.module('TsMetroTheme.interface').
  directive('questionPlot', ['$templateCache', '$log', function($templateCache, $log) {

  var imageMap = angular.fromJson($templateCache.get('TsMetroTheme.interface.questionPlot.imageMap.json'));

  var definitions = function definitions() {
    return {
       science: {
        type: 'scatter',
        name: 'Science',
        color: 'rgba(148, 38, 143, 0.5)',
        data: [],
        pointWidth: 10,
        marker: {
          radius: 8,
          symbol: 'circle'
        }
      },
      mentalability: {
        type: 'scatter',
        name: 'Mental Ability',
        color: 'rgba(13, 112, 181, 0.5)',
        data: [],
        pointWidth: 10,
        marker: {
          radius: 8,
          symbol: 'circle'
        }
      },
      physics : {
        type: 'scatter',
        name: 'Physics',
        color: 'rgba(148, 38, 143, 0.5)',
        data: [],
        pointWidth: 10,
        marker: {
          radius: 8,
          symbol: 'circle'
        }
      },
      chemistry: {
        type: 'scatter',
        name: 'Chemistry',
        color: 'rgba(13, 112, 181, 0.5)',
        data: [],
        pointWidth: 10,
        marker: {
          radius: 8,
          symbol: 'circle'
        }
      },
      mathematics: {
        type: 'scatter',
        name: 'Mathematics',
        color: 'rgba(26, 188, 156, 0.5)',
        data: [],
        pointWidth: 10,
        marker: {
          radius: 8,
          symbol: 'circle'
        }
      },
      biology: {
        type: 'scatter',
        name: 'Biology',
        color: 'rgba(26, 188, 156, 0.5)',
        data: [],
        pointWidth: 10,
        marker: {
          radius: 8,
          symbol: 'circle'
        }
      },
      englishproficiency: {
        type: 'scatter',
        name: 'English Proficiency',
        color: 'rgba(74, 92, 108, 0.5)',
        data: [],
        pointWidth: 10,
        marker: {
          radius: 8,
          symbol: 'circle'
        }
      },
      logicalreasoning: {
        type: 'scatter',
        name: 'Logical Reasoning',
        color: 'rgba(115, 156, 60, 0.5)',
        data: [],
        pointWidth: 10,
        marker: {
          radius: 8,
          symbol: 'circle'
        }
      },
      em: {
        type: 'scatter',
        name: 'Perfect Attempt',
        color: 'rgba(242,156,33, 0.5)',
        data: [],
        pointWidth: 10,
        marker: {
          symbol: 'url(' + imageMap.embibium + ')'
        }
      },
      wa: {
        type: 'scatter',
        name: 'Wasted Attempt',
        color: 'rgba(242,156,33, 0.5)',
        data: [],
        pointWidth: 10,
        marker: {
          symbol: 'url(' + imageMap.trash + ')'
        }
      },
      tt: {
        type: 'scatter',
        name: 'Overtimes',
        color: 'rgba(242,156,33, 0.5)',
        data: [],
        pointWidth: 10,
        marker: {
          symbol: 'url(' + imageMap.timeTroll + ')'
        }
      },
      correct: {
        type: 'scatter',
        name: 'Correct',
        color: 'rgba(242,156,33, 0.5)',
        data: [],
        pointWidth: 10,
        marker: {
          symbol: 'url(' + imageMap.tick + ')'
        }
      },
      wrong: {
        type: 'scatter',
        name: 'Incorrect',
        color: 'rgba(242,156,33, 0.5)',
        data: [],
        pointWidth: 10,
        marker: {
          symbol: 'url(' + imageMap.cross + ')'
        }
      }
    };
  };

  var getChartOptions = function getChartOptions(element, options, data, sections) {
    if(!angular.isObject(options)) { options = {}; }
    if(!angular.isObject(data)) { data = {}; }
    if(!angular.isArray(data)) { sections = []; }

    var sections_series = [];
    angular.forEach(sections, function(section){
      var section_var = section.replace(/ /g,'').toLowerCase();
      if(definitions()[section_var]){
        sections_series.push(definitions()[section_var]);
      }
    });

    var series = {
      series: sections_series.concat([
        definitions().em,
        definitions().wa,
        definitions().tt,
        definitions().correct,
        definitions().wrong
      ])
    };

    var defaultOptions = {
      legend: {
        borderRadius: 0,
        itemDistance: 20,
        itemStyle: {
          fontSize: '10px',
          'text-transform': 'uppercase',
        },
        itemMarginBottom:5,
        itemMarginTop:5,
        padding: 10,
        verticalAlign: 'top',
        itemHoverStyle: {
          color: '#0d70b5'
        }
      },
      chart: {
        type: 'scatter',
        renderTo: element[0],
        width: 940,
        height: 350,
        zoomType: 'x',

      },
      title: {
        text: ''
      },
      xAxis: {
        title: {
          enabled: true,
          text: 'Time of entire test (in minutes)',
          style: {
            fontSize: '12px',
            fontFamily: 'Open Sans',
            fontWeight: '700',
            color: '#000',
          },
          offset: 40
        },
        min: 0,
        max: 180,
        gridLineWidth: 0,
        lineWidth: 1,
        tickLength: 10,
        tickInterval: 15,
        tickColor: '#C0C0C0',
        startOnTick: false,
        endOnTick: false,
        showLastLabel: true
      },
      yAxis: {
        title: {
          text: 'Difficulty level of question'
        },
        min: 0,
        max: 10,
        tickInterval: 2,
        gridLineWidth: 0,
        lineWidth: 1,
        startOnTick: true,
        endOnTick: true,
        showLastLabel: true
      },
      plotOptions: {
        scatter: {
          marker: {
            radius: 5,
            states: {
              hover: {
                enabled: true,
                lineColor: 'rgb(100,100,100)'
              }
            }
          },
          states: {
            hover: {
              marker: {
                enabled: false
              }
            }
          },
          tooltip: {
            shared: true,
            useHTML: true,
            headerFormat: '',
            pointFormat: '<b>{point.section} Q{point.number}</b>' +
              '<br> {point.x} mins., Lvl: {point.y} <br> <b> Time Spent: </b>{point.tt}'
          }
        },
        series: {
          animation: {
            duration: 2000
          }
        }
      },
      series: [],
      credits: {
        enabled: false
      }
    };

    return _.merge(defaultOptions, options, series);
  };

  var extract = function extract(array, key, value) {
    var points = _.filter(array, function gotPoint(point) {
      return point[key] === value;
    });
    var section_map = {
      S: 'Science',
      A: 'Mental Ability',
      P: 'Physics',
      M: 'Mathematics',
      C: 'Chemistry',
      E: 'English Proficiency',
      L: 'Logical Reasoning',
      B: 'Biology',
    };
    return _.map(points, function gotPoint(pt){
      return _.merge({section: section_map[pt.s], number: pt.id }, pt);
    });
  };

  var unsetSeries = function unsetSeries(chart, field) {
    var series =  definitions()[field];
    var chartSeries = _.find(chart.series, function gotSeries(srs) { return srs.name === series.name; });
    if(chartSeries) {
      chartSeries.remove();
    }
  };

  var setSeries = function setSeries(chart, field, data, visible) {
    var series =  definitions()[field];
    var chartSeries = _.find(chart.series, function gotSeries(srs) { return srs.name === series.name; });
    if(chartSeries) {
      chartSeries.setData((data || []), false);
    } else {
      chartSeries = chart.addSeries(_.merge(series, {data: (data || [])}), false);
    }
    if(!visible) { chartSeries.hide(); }
  };

  var update = function update(chart, values, minimal, sections) {
    var data = values;
    if(!angular.isObject(values)) { data = {}; }
    if(!angular.isArray(sections)) { sections = []; }
    //$log.debug('questionPlot updating to', data);
    var sections_extract = {};
    sections_extract['science'] = extract(data, 's', 'S');
    sections_extract['mentalability'] = extract(data, 's', 'A');
    sections_extract['physics'] = extract(data, 's', 'P');
    sections_extract['chemistry'] = extract(data, 's', 'C');
    sections_extract['mathematics'] = extract(data, 's', 'M');
    sections_extract['englishproficiency'] = extract(data, 's', 'E');
    sections_extract['logicalreasoning'] = extract(data, 's', 'L');
    sections_extract['biology'] = extract(data, 's', 'B');
    var em = extract(data, 't', 'EM');
    var wa = extract(data, 't', 'WA');
    var tt = extract(data, 't', 'TT');
    var correct = extract(data, 'c', 1);
    var wrong = extract(data, 'c', -1);
    //var correct = _.difference(extract(data, 'c', 1), em, wa, tt);
    //var wrong = _.difference(extract(data, 'c', -1), em, wa, tt);

    if(minimal) {
      angular.forEach(sections, function(section) {
        var section_var = section.replace(/ /g,'').toLowerCase();
        if(sections_extract[section_var]){
          setSeries(chart, section_var, sections_extract[section_var], true);
        }
      });

      unsetSeries(chart, 'em');
      unsetSeries(chart, 'wa');
      unsetSeries(chart, 'tt');
      unsetSeries(chart, 'correct');
      unsetSeries(chart, 'wrong');
    } else {

      if(sections && sections.length > 1) {
        angular.forEach(sections, function(section) {
          var section_var = section.replace(/ /g,'').toLowerCase();
          if(sections_extract[section_var]){
            unsetSeries(chart, section_var);
            setSeries(chart, section_var, sections_extract[section_var], false);
          }
        });
      }


      unsetSeries(chart, 'em');
      setSeries(chart, 'em', em, false);
      unsetSeries(chart, 'wa');
      setSeries(chart, 'wa', wa, false);
      unsetSeries(chart, 'tt');
      setSeries(chart, 'tt', tt, false);
      unsetSeries(chart, 'correct');
      setSeries(chart, 'correct', correct, true);
      unsetSeries(chart, 'wrong');
      setSeries(chart, 'wrong', wrong, true);
    }
    chart.redraw();
    chart.legend.render();
  };

  return {
    restrict: 'E',
    scope: {
      config: '=',
      values: '=',
      minimal: '=',
      sections: '='
    },
    replace: true,
    element: '<div></div>',
    link: function(scope, element, attrs) {
      var options = getChartOptions(element, scope.config, scope.sections);
      var chart = new Highcharts.Chart(options);
      update(chart, scope.values, scope.minimal, scope.sections);

      scope.$watch('values', function(newValue, oldValue) {
        if(newValue) {
          update(chart, scope.values, scope.minimal, scope.sections);
        }
      }, true);

      scope.$watch('sections', function(newValue, oldValue) {
        if(newValue) {
          update(chart, scope.values, scope.minimal, scope.sections);
        }
      }, true);

      scope.$watch('minimal', function(newValue, oldValue) {
          update(chart, scope.values, scope.minimal, scope.sections);
      });

      scope.$watch('config', function(newValue, oldValue) {
        if (newValue !== oldValue) {
          chart.destroy();
          var options = getChartOptions(element, scope.config, scope.sections);
          var chart1 = new Highcharts.Chart(options);
          chart = chart1;
          update(chart, scope.values, scope.minimal, scope.sections);
        }
      });
    }
  };

}]);