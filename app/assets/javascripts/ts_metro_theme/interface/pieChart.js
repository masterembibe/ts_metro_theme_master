/* global angular: false */
//= require ts_metro_theme/highcharts

(function() {
  'use strict';

  angular
    .module('TsMetroTheme.interface')
    .directive('pieChart', pieChart);

  function pieChart() {
    return {
      restrict: 'E',
      scope: {
        attempts: '=?'
      },
      templateUrl: 'TsMetroTheme.interface.pieChart.html',
      controller: pieChartCtrl
    };
  }

  pieChartCtrl.$inject = ['$scope', '$element'];
  function pieChartCtrl($scope, el) {
    $scope.$watch('attempts', updateChart);

    //// ////
    function updateChart() {
      if(!$scope.chart) { createChart(); }
      window.chart = $scope.chart;
      $scope.data = $scope.attempts;
      $scope.chart.series[0].setData($scope.data, true, false, false);
    }

    function formatter() {
      /* jshint validthis: true */
      var self = this;
      var text;
      if (self.key === 'nodata') {
        text = 'No data Available';
      } else {
        text = this.key  + ': ' + this.y;
      }
      return text;
    }

    function createChart() {
      $scope.chart = new Highcharts.Chart({
        chart: {
          renderTo: el.find('div')[0],
          defaultSeriesType: 'pie',
          backgroundColor: null,
        },
        credits: { enabled: false },
        title: { text: null },
        tooltip: {
          backgroundColor: '#FFF',
          borderColor: '#C0C0C0',
          borderRadius: 0,
          borderWidth: 1,
          shadow: false,
          useHTML: true,
          formatter: formatter
        },
        plotOptions: {
          series: {
            cursor: 'pointer',
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              color: '#000000',
              connectorColor: '#000000',
              formatter: formatter
            }
          },
        },
        series:[{
          data: [],
          point:{
            events:{
              //click: onSelect
            }
          }
        }]
      });
      console.log('Pie chart created');
    }
  }
})();
