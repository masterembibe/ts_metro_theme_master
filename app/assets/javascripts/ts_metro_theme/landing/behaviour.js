/*global $:false, videojs: false */

'use strict';
(function () {

  var hideAllOverlay = function hideAllOverlay() {
    $('.overlay').removeClass('active');
    $('body').removeClass('blurred');
  };

  var disableDbClick = function disableDbClick(){
    $('.signup-form').submit(function(e){
      e.stopPropagation();
      $(this).find('input[type="submit"]').attr('disabled', true);
    })
  }

  var showLogin = function showLogin() {
    hideAllOverlay();
    $('html,body').animate({'scrollTop':0},100);
    $('section#login').addClass('active');
    $('body').addClass('blurred');
    //$('.login').focus();
  };

  var showForgotPassword = function showForgotPassword() {
    hideAllOverlay();
    $('section#forgot-password').addClass('active');
    $('body').addClass('blurred');
    //$('#forgot_password_user_email').focus();
  };

  var showResetPassword = function showResetPassword() {
    hideAllOverlay();
    $('section#reset-password').addClass('active');
    $('body').addClass('blurred');
    //$('#reset_user_password').focus();
  };

  var resetSignup = function resetSignup() {
    $('.above-fold-form').removeClass('wobble');
  };

  var focusSignup = function focusSignup() {
    $('html,body').animate({
      'scrollTop':0
    },300);
    $('.above-fold-form').addClass('wobble');
    //$('#user_first_name').focus();
    setTimeout(resetSignup, 1000);
  };

  var clearHash = function clearHash() {
    var hash = window.location.hash;
    if(hash === "#login" || hash === "#sign-up" || hash === "#forgot-password" || hash === "#reset-password") {
      window.location.hash = "";
    }
  };

  var stopEvent = function stopEvent(e) {
    e.preventDefault();
    e.stopPropagation();
  };

  var pricingNavBind = function pricingNavBind(e) {
    $('.tab-nav__pricing button').click(function(e){
      e.stopPropagation();
      var tab = $(this).data('tab');
      $(this).addClass('active').siblings().removeClass('active');
      $('.pricing-table').removeClass('active');
      $('.pricing-table.'+tab).addClass('active');
    })
  };

  // Login form is by default shown.
  var setupLoginButton = function setupLoginButton() {
    var hash = window.location.hash;
    if(hash === "#login") { showLogin(); }
    $('.login-button').click(function loginClicked(e) {
      stopEvent(e);
      resetSignup();
      showLogin();
      clearHash();
    });
  };

  var setupSignupButton = function setupSignupButton() {
    var hash = window.location.hash;
    var content = $('#user_first_name').val() + $('#user_last_name').val() + $('#user_email').val() + $('#user_mobile').val();
    if(hash === "#sign-up" || content !== "" ) {
      focusSignup();
    }
    $('.header-signup-button,.fake-signup-btn,.footer-signup-button,ul.pricing-table li').click(function signupClicked(e) {
      stopEvent(e);
      focusSignup();
      clearHash();
    });
  };

  var setupForgotPasswordButton = function setupForgotPasswordButton() {
    var hash = window.location.hash;
    var content = $('#forgot_password_user_email').val();
    if(hash === "#forgot-password" || content !== "" ) {
      showForgotPassword();
    }
    $('.forgot-password-tooltip').click(function forgotPasswordClicked(e) {
      stopEvent(e);
      showForgotPassword();
      clearHash();
    });
  };

  var setupResetPasswordButton = function setupResetPasswordButton() {
    var hash = window.location.hash;
    if(hash === "#reset-password") {
      showResetPassword();
    }
  };

  var setupSectionNav = function setupSectionNav() {
    $('[data-section]').click(function(e){
      stopEvent(e);
      var scrollTo = $('#'+$(this).data('section')).offset().top - 20;
      $('html,body').animate({'scrollTop':scrollTo},200);
    });
  };


  var setupOverlayClose = function setupOverlayClose() {
    $(document).on('click',function(e){
      e.stopPropagation();
      if($(e.target).hasClass('overlay') || $(e.target).hasClass('overlay-close')){
        $('body').removeClass('blurred');
        $('.overlay').removeClass('active');
      }
      $('.embibe-select ul').removeClass('active');
    });
  };

  var loginClick = function loginClick() {
    $('button.signin-button').click(function(){
      if( $.trim($('#user_login').val()) !== "" && $.trim($('#user_password').val()) !== "" ) {
        $(this).find('span').html('Signing in..');
      }
    });
  };

  var showVideoOverlay = function showVideoOverlay() {
    $('ul.team-members li a').on('click',function(e){
      e.stopPropagation();
      var video = $('<iframe>').addClass('overlay-container')
      .attr({
        src: $(this).data('video')?$(this).data('video'):"//player.vimeo.com/video/65925315",
        width:500,
        height:281,
        frameborder:0,
        webkitallowfullscreen:true,
        allowfullscreen:true,
        mozallowfullscreen:true
      });
      var overlayClose = $('<button>').addClass('overlay-close-btn').html('x');
      $('section#video-overlay').html(video).append(overlayClose);
      $('section#video-overlay').addClass('active');
      $('body').addClass('blurred');
      setTimeout(function(){$('.landing-video').addClass('animate-drop'); },300);
    });
  };

  var bindDownArrow = function bindDownArrow() {
    var anchorPoints = [
          $('.how-it-works').offset().top,
          $('section.teachers').offset().top,
          $('section.ultimate-test-series').offset().top,
          $('section.testimonial').offset().top
           ];

    $('.down-arrow').click(function(e){
      e.stopPropagation();
      var hasScrolled = $(window).scrollTop();
      var Continue = false;
      for (var i = 0; i <= anchorPoints.length - 1; i++) {
        if( anchorPoints[i] > hasScrolled + 80 && !Continue ) {
          $('html,body').animate({'scrollTop' : anchorPoints[i] - 60 }, 300);
          Continue = true;
        }
      };
    })

    $('.up-arrow').click(function(e){
      e.stopPropagation();
      $('html,body').animate({'scrollTop' : 0 }, 600);
    });
  }

  var bindScrollArrow = function bindDownArrow() {
    var anchorPoints = [];
    $('[scroll-anchor]').each(function(){
      anchorPoints.push($(this).offset().top);
    });
    console.log(anchorPoints)
    $('.down-arrow').unbind('all');
    $('.down-arrow1').click(function(e){
      e.stopPropagation();
      var hasScrolled = $(window).scrollTop();
      var Continue = false;
      for (var i = 0; i <= anchorPoints.length - 1; i++) {
        if( anchorPoints[i] > hasScrolled + 80 && !Continue ) {
          $('html,body').animate({'scrollTop' : anchorPoints[i] - 60 }, 300);
          Continue = true;
        }
      };
    })
    $('.up-arrow').unbind('all');
    $('.up-arrow1').click(function(e){
      e.stopPropagation();
      $('html,body').animate({'scrollTop' : 0 }, 600);
    });
  }

  $(document).on('click','.overlay-close-btn',function(e){
    e.stopPropagation();
    $('section#video-overlay').html('');
    $('section#video-overlay').removeClass('active');
    $('body').removeClass('blurred');
  });

  var testiData = {
    '1' : {
      'name':'Arjun Rao',
      'city':'Mumbai, Maharashtra',
      'rank':'216',
      'challenge' : 'The challenge of accuracy, of not making silly mistakes and of maintaining a steady focus in the examination.',
      'last2months' : 'The student must attempt several sample papers in order to develop focus and learn time management.',
      'favfeature' : 'The feature of weakness detection was a very helpful feature for me.',
      'advice' : 'Study, Study Hard. Study because you love to study.',
      'videoId':'85818152'
    },
    '2' : {
      'name': 'Priyansh Tiwari',
      'city':'Kota, Rajasthan',
      'rank': '337',
      'challenge' : 'Lost in a crowd of students and high hopes of parents. I didn\'t know by strengths and weaknesses, wasn\'t sure if i can event get into it.',
      'last2months' : 'Study and revise effectively. You should have best and precise material and attempt full length question paper in proper environment',
      'favfeature' : 'I love the how embibe focuses on the overall interaction between student, teacher and parents. Then focus on individual experience & work on improving each point of a student and not just academics',
      'advice' : 'Study hard but more importantly be confident. Don\'t underestimate time management. A.I.R-1 is no-one special, he is just one of you, may just be a bit more focussed and somewhat better guidance, So don\'t underestimate yourself & dream big',
      'videoId':'86291172'
    },
    '3' : {
      'name':'Ankit Ranjan',
      'rank':'295',
      'city':'Patna, Bihar',
      'challenge' : 'The biggest challenge i faced while preparing for JEE was to grasp concepts in chemistry. Mainly salt analysis in Inorgainc chemistry.',
      'last2months' : 'In the last 2 months before JEE, a student should take tests on their own, solve papers of JEE as many as they can. This helps a lot in dealing with nervousness.',
      'favfeature' : 'My favourite Embibe feature is th personalized analysis of a student performance. It helps to know whether a student is on the right path or not.',
      'advice' : 'My advice for this year\'s JEE is aspirants is to study daily, revise what you study and keep practicing the material.',
      'videoId':'86291091'
    },
    '4' : {
      'name':'Maharshi Yadav',
      'rank':'187',
      'city':'Satara, Maharashtra',
      'challenge' : 'Clearing all concepts. I had no background of my weak concepts and type of questions i go wrong. In 12th as i was serious about JEE, i had no idea where i stood in the bulk of 5-6 lakh students',
      'last2months' : 'Time management. Able to solve paper more effectively. Solve easy questions and then go for hard ones. Figure out what approach leads you for maximum marks in a paper',
      'favfeature' : 'The feature of letting me know in what concepts i am lacking. Quest of the day: I would love to challenge the good performing students and win',
      'advice' : 'During preparation make weekly goals and fulfill in accordingly. Study hours should be pre-planned and should be done seriously whenever it is done. Start solving papers by changing your venue.',
      'videoId':'86290786'
    },
    '5' : {
      'name':'Ishan Anand',
      'rank':'58',
      'city':'Muzaffarpur, Bihar',
      'challenge' : 'Going through stuff like chemistry with more exeptions than general rules. Maintaining motivation throughout the preperation period.',
      'last2months' : 'Practice in actual test environment like timed mock tests etc. Revision through the topics you\'ve covered',
      'favfeature' : 'The interface is sleek and intuitive and personalized and provides regular feedback. Measuring you on a lot of metrics like speed, accuracy and areas of weakness',
      'advice' : 'Don\'t freak out just because the exams are near, Stay calm. Practice enough mock tests.',
      'videoId':'86294358'
    },
    '6' : {
      'name':'Partha Bhattacharya',
      'city':'Pune, Maharashtra',
      'rank':'196',
      'challenge' : 'Persistance did not come easily, and attention span. Failing to solve problems',
      'last2months' : 'Solve atleast 50 question papers in last 2 months, take rest for the last 10 days.',
      'favfeature' : 'There are two features i like the most, one being the oppurtunity to challenge other toppers and the other is the fact that embibe has collected a lot of info before making this test series.',
      'advice' : 'Keep calm and study hard',
      'videoId':'86293971'
    },
    '7' : {
      'name':'Dhruv Nigam',
      'city':'Mumbai, Maharashtra',
      'rank':'332',
      'challenge' : 'My biggest challenge was working more than 2 hours a day. I was used to studying just before the exams but for JEE i need discipline and work almost 6-8 hours everyday.',
      'last2months' : 'Assuming he/she has already dont with the concepts, I\'d advice him to go over all the good questions.',
      'favfeature' : 'The parent feedback certainly is a very strong feature which gives the parents the level of information which enables them to be an active participant in their child\'s learning and be supportive.',
      'advice' : 'In the end, it is all about motivation. You have to ask how much you want to get into a good college.',
      'videoId':'86291166'
    }
  };

  var testimonialStudentSelect = function(){

    $('nav.students a').click(function(e){

      e.stopPropagation();
      var $this = $(this);
      var id = $this.data('key');

      //testiVideo.play();

      $this.addClass('active').siblings().removeClass('active');
      $('p#testi-challenge').html(testiData[id].challenge);
      $('p#testi-last2months').html(testiData[id].last2months);
      $('p#testi-favfeature').html(testiData[id].favfeature);
      $('h4#testi-name').html(testiData[id].name);
      $('span.city').html(testiData[id].city);
      $('span#testi-rank').html(testiData[id].rank);
      $('p#testi-advice').html(testiData[id].advice);
      $('section#active-testimonial iframe').remove();
      $('section#active-testimonial').append('<iframe src="http://player.vimeo.com/video/'+testiData[id].videoId+'?api=1&badge=0&amp;color=0d70b5" width="888" height="500" frameborder="0"></iframe>');
      $('section#active-testimonial .fat-play-btn').removeClass('hidden');
    });
  };

  var triggerMouseOver = function(){
    var mouseEnterIns = null;

    $('ul.team-members a').hover(
      function(){
        var $this = $(this);
        mouseEnterIns = setTimeout(function(e){
          $this.addClass('hover');
        },50)
      },
      function(){
        clearTimeout(mouseEnterIns);
        $(this).removeClass('hover');
      }
    );
  }
  var scrollCheck = function scrollCheck(){
    var timer = 0,
        scrollTop = 0,
        bodyHeight = $('body').height();

    var handler = function(){
      timer = 0;
      scrollTop = $(window).scrollTop();
      if( scrollTop + 300 > bodyHeight - 500 ) {
        $('.down-arrow').addClass('hidden');
        $('.up-arrow').removeClass('hidden');
      } else {
        $('.up-arrow').addClass('hidden');
        $('.down-arrow').removeClass('hidden');
      }
    }

    $(window).scroll(function(e) {
      if (timer) {
          clearTimeout(timer);
          timer = 0;
      }
      timer = setTimeout(handler, 200);
    });
  }

  var showAnswerKey = function showAnswerKey(){
    $('.showAnswerKey').click(function(){
      $('.key-overlay#'+$(this).attr('id')).addClass('active');
    })
  }

  var bindArrows = function() {
    $('.key-overlay .fa-chevron-left').click(function(){
      var newIndex = $('.key-overlay .active').index() - 2;
      console.log($('.key-overlay ul li:nth-child('+newIndex+')'))
      $('.key-overlay ul li:nth-child('+newIndex+')').addClass('active').siblings().removeClass('active');
    })
    $('.key-overlay .fa-chevron-right').click(function(){
      var newIndex = $('.key-overlay .active').index() + 2;
      console.log($('.key-overlay ul li:nth-child('+newIndex+')'));
      $('.key-overlay ul li:nth-child('+newIndex+')').addClass('active').siblings().removeClass('active');
    })
  }

  var arrowClick = function arrowClick() {
    $('.select-arrow').click(function() {
        var e = document.createEvent("MouseEvents");
        e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        $('#user_primary_goal_name')[0].dispatchEvent(e);
    });
  }

  var bindDemoVideo = function bindDemoVideo() {
    $('section.feedback-video video').click(function(){
      var video = $(this)[0];
      if (video.paused) {
        video.play();
        $('.fat-play-btn').addClass('hidden');
      } else {
        video.pause();
        $('.fat-play-btn').removeClass('hidden');
      }
    });

    $('.fat-play-btn').click(function(){
      $('section.feedback-video video')[0].play();
      $('.fat-play-btn').addClass('hidden');
    })
  }

  var testiVideo = null;

  var CITIES = ["Mumbai","Delhi","Bangalore","Hyderabad","Ahmedabad","Chennai","Kolkata","Surat","Pune","Jaipur","Lucknow","Kanpur","Nagpur","Indore","Thane","Bhopal","Visakhapatnam","Pimpri-Chinchwad","Patna","Vadodara","Ghaziabad","Ludhiana","Agra","Nashik","Faridabad","Meerut","Rajkot","Kalyan-Dombivali","Vasai-Virar","Solapur","Varanasi","Srinagar","Aurangabad","Dhanbad","Amritsar","Navi Mumbai","Allahabad","Ranchi","Howrah","Coimbatore","Jabalpur","Gwalior","Vijayawada","Jodhpur","Madurai","Raipur","Kota","Guwahati","Chandigarh","Hubballi-Dharwad","Bareilly","Moradabad","Mysore","Gurgaon","Aligarh","Jalandhar","Tiruchirappalli","Bhubaneswar","Salem","Mira-Bhayandar","Trivandrum(Thiruvananthapuram)","Bhiwandi","Saharanpur","Gorakhpur","Guntur","Bikaner","Amravati","Noida","Jamshedpur","Bhilai","Warangal","Mangalore","Cuttack","Firozabad","Kochi (Cochin)","Bhavnagar","Dehradun","Durgapur","Asansol","Nanded","Kolhapur","Ajmer","Gulbarga","Jamnagar","Ujjain","Loni","Siliguri","Jhansi","Ulhasnagar","Nellore","Jammu","Sangli-Miraj & Kupwad","Belgaum","Ambattur","Tirunelveli","Malegaon","Gaya","Jalgaon","Udaipur","Maheshtala","Tirupur","Davanagere","Kozhikode (Calicut)","Akola","Kurnool","Rajpur Sonarpur","Bokaro","South Dumdum","Bellary","Patiala","Gopalpur","Agartala","Bhagalpur","Muzaffarnagar","Bhatpara","Panihati","Latur","Dhule","Rohtak","Korba","Bhilwara","Brahmapur","Muzaffarpur","Ahmednagar","Mathura","Kollam (Quilon)","Avadi","Rajahmundry","Kadapa","Kamarhati","Bilaspur","Shahjahanpur","Bijapur","Rampur","Shivamogga(Shimoga)","Chandrapur","Junagadh","Thrissur","Alwar","Bardhaman","Kulti","Kakinada","Nizamabad","Parbhani","Tumkur","Hisar","Ozhukarai","Bihar Sharif","Panipat","Darbhanga","Bally","Aizawl","Dewas","Ichalkaranji","Tirupati","Karnal","Bathinda","Jalna","Barasat","Kirari Suleman Nagar","Purnia","Satna","Mau","Sonipat","Farrukhabad","Sagar","Rourkela","Durg","Imphal","Ratlam","Hapur","Anantapur","Arrah","Karimnagar","Etawah","Ambernath","North Dumdum","Bharatpur","Begusarai","New Delhi","Gandhidham","Baranagar","Tiruvottiyur","Puducherry","Sikar","Thoothukudi","Rewa","Mirzapur","Raichur","Pali","Ramagundam","Vizianagaram","Katihar","Haridwar","Sri Ganganagar","Karawal Nagar","Nagercoil","Mango","Bulandshahr","Thanjavur"];

  var olarkData = function(details) {
    $('#user_olark_details').val(JSON.stringify(details));
  };

  /*var collegeTagCloud = function collegeTagCloud(){
    console.log($('#tag-cloud'))
    $('#tag-cloud').jQCloud([
        {text: "IIT Delhi", weight: 15},
        {text: "NSIT", weight: 12},
        {text: "NIT Warangal ", weight: 9},
        {text: "Visvesvaraya National Institute of Technology", width: 8},
        {text: "BITS Mesra", weight: 6},
        {text: "Delhi College of Engineering", weight: 7},
        {text: "Motilal Nehru National Institute of Technology", weight: 5}
    ])
  }*/

  $(document).ready(function onReady() {
    try {
      olark('api.visitor.getDetails', olarkData);
    } catch(e) {}
    setupLoginButton();
    setupSignupButton();
    setupForgotPasswordButton();
    setupResetPasswordButton();
    pricingNavBind();
    setupSectionNav();
    setupOverlayClose();
    showAnswerKey();
    disableDbClick();
    bindDemoVideo();
    bindArrows();
    showVideoOverlay();
    triggerMouseOver();
    loginClick();
    arrowClick();
    console.log($('.up-arrow1').length)
    if($('.up-arrow1').length) {
      bindScrollArrow();
    }
    try {
      bindDownArrow();
    } catch(e) {}
    scrollCheck();
    testimonialStudentSelect();
    setTimeout(resetSignup, 1000);
    $('#user_city').typeahead({
      source : CITIES
    });
    $('#user_category').typeahead({
      source : ["OBC","SC","ST", "GENERAL"]
    });
  });

  var pricingBind = function pricingBind(){
    $('.pricing__card-year').click(function(){
      var $this = $(this);
      var newPrice = $this.data('price');
      var discount = $this.data('discount');
      if(discount) {
        $this.parents('.pricing__card').find('.pricing__discount').html(discount).addClass('pricing__discount--active');
      } else {
        $this.parents('.pricing__card').find('.pricing__discount').removeClass('pricing__discount--active');
      }
      $this.addClass('pricing__card-year--active').siblings().removeClass('pricing__card-year--active');
      $this.parents('.pricing__card').find('.pricing__price').html(newPrice);
    });

    $('.pricing__nav-link').click(function(){
      var $this = $(this);
      var section = $this.data('pricing');
      $this.addClass('pricing__nav-link--active').siblings().removeClass('pricing__nav-link--active');
      $('.row__pricing[data-sec='+section+']').addClass('row__pricing--active').siblings().removeClass('row__pricing--active');
    });
  }

  var bindPricingAccordion = function bindPricingAccordion(){
    $('.pricing__accordion-header').click(function(){
      var $this = $(this);
      $this.parent().find('.pricing__accordion-header').removeClass('active');
      $this.addClass('active');
    });
    $('.pricing__inner-accordion-header').click(function(){
      var $this = $(this);
      $this.parent().find('.pricing__inner-accordion-header').removeClass('active');
      $this.addClass('active');
    });

  }

})();

$(document).ready(function(){
  $('.pricing__accordion-header').click(function(){
    var $this = $(this);
    $this.parent().find('.pricing__accordion-header').removeClass('active');
    $this.addClass('active');
  });
  $('.pricing__inner-accordion-header').click(function(){
    var $this = $(this);
    $this.parent().find('.pricing__inner-accordion-header').removeClass('active');
    $this.addClass('active');
  });
  $('.pricing__nav-link').click(function(){
    var $this = $(this);
    var section = $this.data('pricing');
    $this.addClass('pricing__nav-link--active').siblings().removeClass('pricing__nav-link--active');
    $('.row__pricing[data-sec='+section+']').addClass('row__pricing--active').siblings().removeClass('row__pricing--active');
  });
});

$(document).on('keydown',function(e){
  e.stopPropagation();
  if(e.which === 27){
    $('body').removeClass('blurred');
    $('.overlay').removeClass('active');
    $('section#video-overlay').html('');
  }
});

// jQuery plugin to prevent double submission of forms
jQuery.fn.preventDoubleSubmission = function() {
  $(this).on('submit',function(e){
    var $form = $(this);

    if ($form.data('submitted') === true) {
      // Previously submitted - don't submit again
      e.preventDefault();
    } else {
      // Mark it so that the next submit can be ignored
      $form.data('submitted', true);
    }
  });

  // Keep chainability
  return this;
};

$(document).on('click', "[ga-track]", function(e) {
  var track_vars = $(this).attr("ga-track").split(",");
  try {
    ga('send', 'event', track_vars[0], track_vars[1], track_vars[2]);
  } catch(e){}
});
