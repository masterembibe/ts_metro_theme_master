$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "ts_metro_theme/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "ts_metro_theme"
  s.version     = TsMetroTheme::VERSION
  s.authors     = ["embibe"]
  s.email       = ["techsupport@embibe.com"]
  s.homepage    = "http://finalstep.embibe.com"
  s.summary     = "A theme for embibe's Test Series"
  s.description = "A metro ui theme for embibe's Test Series product"
  s.license     = "All Rights reserved."

  s.files = Dir["{app,config,db,lib,vendor}/**/*", "Rakefile", "README.rdoc"]
  s.test_files = []

  s.add_dependency "rails", ">= 4.1.6"
  s.add_dependency "slim-rails", ">= 2.0"
  s.add_dependency "sass-rails", ">= 4.0.1"
  s.add_development_dependency "sqlite3", "~> 1.3.8"
  s.add_development_dependency "mathjax-rails", "~> 2.3.0"
  s.add_development_dependency "jquery-rails"
  s.add_development_dependency "foreman", "~> 0.63"
  s.add_development_dependency "awesome_print", "~> 1.2"
  s.add_development_dependency "bundler", "~> 1.3"
  s.add_development_dependency "rspec", "~> 3.0.0.beta1"
  s.add_development_dependency "rspec-given", "~> 3.3.0"
  s.add_development_dependency "rake"

  s.add_development_dependency "better_errors"
  s.add_development_dependency "binding_of_caller"
  s.add_development_dependency "meta_request"
  s.add_development_dependency "pry-rails"

  # For heroku
  s.add_development_dependency "uglifier"

  # Live reload
  s.add_development_dependency "guard"
  s.add_development_dependency "guard-livereload"
  s.add_development_dependency "rack-livereload"

end
